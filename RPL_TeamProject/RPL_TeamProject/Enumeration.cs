﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_TeamProject
{
    class Enumeration
    {
        public enum DataVerbs
        {
            Get = 1,
            Post = 2,
            Put = 3,
            Delete = 4,
        }
    }
}
