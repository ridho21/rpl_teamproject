﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using RPL_TeamProject.ViewModels;
using RPL_TeamProject.Models;

namespace RPL_TeamProject.Views.Product {
    public partial class ProductView : Window {
        private readonly ProductViewModel vm;

        public ProductView() {
            InitializeComponent();
            vm = new ProductViewModel();
            vm.OnPushComponent += ViewModel_PushComponent;
            DataContext = vm;
        }

        private void FrmProduct_Loaded(object sender, RoutedEventArgs e) {
            ViewModel_PushComponent(new object(), new PushComponentEvent());
        }

        private void ViewModel_PushComponent(object sender, PushComponentEvent e) {
            TblDataGrid.SelectedItem = new ProductModel();
            TxtCode.Visibility = Visibility.Hidden;
            TxtName.Visibility = Visibility.Hidden;
            TxtPrice.Visibility = Visibility.Hidden;
            TxtGuarantee.Visibility = Visibility.Hidden;
            BtnSave.Visibility = Visibility.Hidden;
            BtnUpdate.Visibility = Visibility.Hidden;
            BtnDelete.Visibility = Visibility.Hidden;
            BtnNew.Visibility = Visibility.Visible;
        }

        private void TblDataGrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e) {
            TxtCode.Visibility = Visibility.Visible;
            TxtName.Visibility = Visibility.Visible;
            TxtPrice.Visibility = Visibility.Visible;
            TxtGuarantee.Visibility = Visibility.Visible;
            BtnUpdate.Visibility = Visibility.Visible;
            BtnDelete.Visibility = Visibility.Visible;
            TxtName.Focus();
        }

        private void Txt_KeyDown(object sender, KeyEventArgs e) {
            if (sender.Equals(TxtCode)) {
                if (e.Key == Key.Enter) {
                    TxtName.Focus();
                } else if (e.Key == Key.Escape) {
                    this.Close();
                }
            } else if (sender.Equals(TxtName)) {
                if (e.Key == Key.Enter) {
                    TxtPrice.Focus();
                } else if (e.Key == Key.Escape) {
                    this.Close();
                }
            } else if (sender.Equals(TxtPrice)) {
                if (e.Key == Key.Enter) {
                    TxtGuarantee.Focus();
                } else if (e.Key == Key.Escape) {
                    TxtName.Focus();
                }
            } else if (sender.Equals(TxtGuarantee)) {
                if (e.Key == Key.Enter) {
                    if (BtnSave.Visibility == Visibility.Visible) {
                        BtnSave.Focus();
                    } else if (BtnUpdate.Visibility == Visibility.Visible) {
                        BtnUpdate.Focus();
                    }
                } else if (e.Key == Key.Escape) {
                    TxtPrice.Focus();
                }
            }
        }

        private void Txt_GotFocus(object sender, RoutedEventArgs e) {
            var bcolor = new BrushConverter();
            if (sender.Equals(TxtCode)) {
                TxtCode.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            } else if (sender.Equals(TxtName)) {
                TxtName.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            } else if (sender.Equals(TxtPrice)) {
                TxtPrice.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            } else if (sender.Equals(TxtGuarantee)) {
                TxtGuarantee.Background = (Brush)bcolor.ConvertFrom("#FFFFF9A9");
            }
        }

        private void Txt_LostFocus(object sender, RoutedEventArgs e) {
            var bcolor = new BrushConverter();
            if (sender.Equals(TxtCode)) {
                TxtCode.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            } else if (sender.Equals(TxtName)) {
                TxtName.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            } else if (sender.Equals(TxtPrice)) {
                TxtPrice.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            } else if (sender.Equals(TxtGuarantee)) {
                TxtGuarantee.Background = (Brush)bcolor.ConvertFrom("#FFFFFFFF");
            }
        }

        private void BtnNew_Click(object sender, RoutedEventArgs e) {
            TxtCode.Visibility = Visibility.Visible;
            TxtName.Visibility = Visibility.Visible;
            TxtPrice.Visibility = Visibility.Visible;
            TxtGuarantee.Visibility = Visibility.Visible;
            BtnSave.Visibility = Visibility.Visible;
            TxtCode.Text = TxtName.Text = TxtPrice.Text = TxtGuarantee.Text = string.Empty;
            TxtCode.Focus();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }
    }
}
