﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_TeamProject.Data
{
    public class Orders : Entity
    {
        public override string Id { get; set; }
        public string CurrDate { get; set; }
        public string Customer { get; set; }
        public string Total { get; set; }
    }
}
