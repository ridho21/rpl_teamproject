﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_TeamProject.Data
{
    public class OrdersDetail : Entity
    {
        public override string Id { get; set; }
        public Orders Orders { get; set; } = new Orders();
        public string Qty { get; set; }
        public string Amount { get; set; }
    }
}
